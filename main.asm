%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUFFER_SIZE 255
%define SYS_WR 1
%define STD_ERR 2
%define CODE_ERR_READ 3
%define CODE_ERR_FOUND 4


section .bss
buffer: resb (BUFFER_SIZE + 1)

section .rodata
err_1_notread:
db "the line was not read", 10, 0
err_2_notfound:
db "element is not found", 10, 0

section .text

global _start

_start:
    mov     rdi, buffer
    mov     rsi, BUFFER_SIZE
    call    read_word
    test    rax, rax
    jnz .next
.err_not_read:
    mov      rdi, err_1_notread
    call     print_err
    mov      rdi, CODE_ERR_READ
    call     exit
.err_not_found:
    mov     rdi, err_2_notfound
    call    print_err
    mov     rdi, CODE_ERR_FOUND
    call    exit
.next:
    mov     rsi, DICTIONARY
    mov     rdi, buffer
    call    find_word  
    test    rax, rax
    jz      .err2
    mov     rdi, rax
    add     rdi, 8
    push    rdi    
    call    string_length
    pop     rdi
    add     rdi, rax
    inc     rdi
    call    print_string
    xor	    rdi, rdi
    call    exit
print_err:
    push    rdi
    call    string_length
    pop     rsi
    mov     rdx, rax
    mov     rax, SYS_WR
    mov     rdi, STD_ERR
    syscall
    ret
