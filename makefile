PYC = python3
ASM = nasm
ASM_FLAGS = -felf64
main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

program: main.o lib.o dict.o
	ld -o $@ $^

clean:
	rm *.o
test:
	$(PYC) test.py

.PHONY: clean test


