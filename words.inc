%include "colon.inc"

section .rodata

colon "third", thirdWord
db "third_string", 0

colon "second", secWord
db "second_string", 0

colon "first", firstWord
db "first_string", 0

%define DICTIONARY firstWord
