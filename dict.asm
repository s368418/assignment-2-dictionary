%include "lib.inc"

global find_word

find_word:
	push r12
	push r13
.loop:	
	mov r12, rsi
	add rsi, 8
	call string_equals
	test rax, rax
	jz .nexts
	mov rax, r12
	pop r12
	pop r13
	ret
.nexts:
	mov rsi, [r12]
	test rsi, rsi
	jnz .loop
	xor rax, rax
	pop r12
	pop r13
	ret
	
	
