
global exit
global string_lenght
global print_string
global print_char
global print_int
global print_uint
global print_newline
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
%define SYSEX 60
%define SYSWR 1
%define SYSRD 0
%define STDOUT 1
%define STDIN 0
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, SYSEX
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor     rax, rax
    .loop:
        cmp     byte[rax+rdi], 0
        je      .end
        inc     rax
        jmp     .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push    rdi
        call    string_length
        pop     rsi
        mov     rdx, rax
        mov     rax, SYSWR
        mov     rdi, STDOUT
        syscall
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov     rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push    rdi
    mov     rax, SYSWR
    mov     rdi, STDOUT
    mov     rsi, rsp
    mov     rdx, 1
    syscall
    add     rsp, 8
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
        test    rdi, rdi
        jns     print_uint
        push    rdi
        mov     rdi, '-'
        call    print_char
        pop     rdi
        neg     rdi
        
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        mov     r9, 10
        mov     rax, rdi
        mov     rcx, rsp
        sub     rsp, 32
        dec     rcx
        mov     byte[rcx], 0
.loop:
        xor     rdx, rdx
        div     r9
        add     rdx, '0'
        dec     rcx
        mov     byte[rcx], dl
        test    rax, rax
        jne     .loop
.print:
        mov     rdi, rcx
        call    print_string
        add     rsp, 32
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        xor     rcx, rcx
    .loop:
        mov     r9b, byte[rdi+rcx]
        cmp     r9b, byte [rsi+rcx]
        jne     .no_equal
    .check:
        test    r9b, r9b
        je      .equal
        inc     rcx
        jmp     .loop
    .no_equal:
        xor     rax,rax
        ret
    .equal:
        mov     rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push    0
        mov     rax, SYSRD
        mov     rdi, STDIN
        mov     rsi, rsp
        mov     rdx, 1
        syscall
        pop rax
        ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	    push    r12
        push    r13
        push    r14
        mov     r12, rdi
        mov     r13, rsi
        xor     r14, r14
.loop:        
        call    read_char
        cmp     r13, r14
        jng     .err
        cmp     rax, 0
        je      .done
        cmp     rax, `\t`
        je      .skip
        cmp     rax, ` `
        je      .skip
        cmp     rax, `\n`
        je      .skip
        mov     [r12+r14], al
        inc     r14
        jmp     .loop
.skip:
        test    r14, r14
        je      .loop
.done:
        mov     byte[r12+r14], 0
        mov     rax, r12
        mov     rdx, r14
        jmp     .end
.err:
        xor     rax, rax
.end:
        pop     r14
        pop     r13
        pop     r12
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor     rax, rax        			
    xor     rdx, rdx        			
    .loop:
        mov      r8b, byte [rdi+rdx]  		
        cmp      r8b, '0'				
        jb       .done               
        cmp      r8b, '9'
        ja       .done               
	    inc      rdx 
        imul     rax, rax, 10      			
        sub      r8b, '0'           		
        add      rax, r8           			          
        jmp      .loop
    .done:
        ret   

; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
    xor     rax, rax
    xor     rdx, rdx
    cmp     byte[rdi], '-'
    je      .mns
    cmp     byte[rdi], '+'
    je      .pls
    jmp     parse_uint
.mns:
    inc     rdi
    call    parse_uint
    neg     rax
    inc     rdx
    ret
.pls:
    inc     rdi
    call    parse_uint
    inc     rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor     rax, rax				
    .loop:
    	cmp     rax, rdx				
    	jge     .false
    	mov     r8b, byte [rdi+rax]
    	mov     byte [rsi+rax], r8b			
    	test    r8b, r8b		
    	inc     rax				
    	jne     .loop 
    	ret
    .false:
    	xor     rax, rax
    	ret
